<?php

namespace Yicr\SimpleJsonApiClient {

    class Client
    {
        /**
         * @var string
         */
        private $version = '1.0.0';

        /**
         * @var string
         */
        private $ua = 'PHP-SIMPLE-JSON-API-Client/1.0.0';

        /**
         * @var resource curl channel resource
         */
        private $channel = null;

        /**
         * @var string
         */
        private $host = "api.example.com";

        /**
         * @var string
         */
        private $scheme = 'http';

        /**
         * @var int
         */
        private $port = 0;

        /**
         * @var string
         */
        private $path = '';

        /**
         * @var string
         */
        public $url = '';

        /**
         * @var array
         */
        private $options = array();

        private $headers = array(
            'Accept: application/json',
            'Accept-Charset: utf-8'
        );

        private $headerSetCallback = null;

        /**
         * Client constructor.
         *
         * @param string $host hostname.
         * @param string $scheme network protocol.
         * @param int $port port number.
         * @param string $path directory path.
         */
        public function __construct($host, $scheme = 'http', $port = 0, $path = '')
        {
            $this->host = $host;
            $this->scheme = $scheme;
            $this->port = $port;
            $this->path = $path;
            $system = php_uname('s');
            $machine = php_uname('m');
            $pv = phpversion();
            $cv = curl_version();
            $this->ua = sprintf('Mozilla/5.0 (%s %s) PHP/%s Curl/%s Client/%s', $system, $machine, $pv, $cv['version'], $this->version);
        }

        /**
         * set/get option
         *
         * @param $name
         * @param null $value
         * @return mixed|null
         */
        public function option($name, $value = null)
        {
            if (func_num_args() > 1) {
                return $this->options[$name] = $value;
            } else {
                return isset($this->options[$name]) ? $this->options[$name] : null;
            }
        }

        /**
         * set options
         *
         * @param array $options
         * @return array
         */
        public function options(array $options)
        {
            return $this->options += $options;
        }

        /**
         * header set callback.
         *
         * @param callable $callback callable function
         * @return void
         * @throws \RuntimeException
         */
        public function header($callback)
        {
            if (is_callable($callback)) {
                $this->headerSetCallback = $callback;
            } else {
                throw new \RuntimeException('invalid callback argument', 4001);
            }
        }

        /**
         * call api.
         *
         * @param string $endpoint endpoint
         * @param array $params request params
         * @param string $method http request method Get/Post
         * @return mixed
         * @throws \RuntimeException
         */
        public function call($endpoint = '/path/to/endpoint', $params = array(), $method = 'GET')
        {
            $endpoint = strtolower($endpoint);
            $endpoint = (substr($endpoint, 0, 1) != '/') ? sprintf('/%s', $endpoint) : $endpoint;

            $this->channel = curl_init();
            curl_setopt($this->channel, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($this->channel, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($this->channel, CURLOPT_TIMEOUT, 60);

            if ($this->ua) {
                curl_setopt($this->channel, CURLOPT_USERAGENT, $this->ua);
            }

            curl_setopt($this->channel, CURLOPT_CUSTOMREQUEST, $method);
            $query = false;
            if ($method == 'GET') {
                if (empty($params) !== false) {
                    $query = http_build_query($params);
                }
            } else {
                $json = json_encode($params, JSON_FORCE_OBJECT);
                curl_setopt($this->channel, CURLOPT_POSTFIELDS, $json);
                if ($method == 'POST') {
                    curl_setopt($this->channel, CURLOPT_POST, true);
                }
            }

            if (!empty($this->options)) {
                foreach ($this->options as $key => $val) {
                    list($target, $option) = explode('.', $key);
                    if ($target === 'CURL' && defined($curlOption = ('CURLOPT_' . $option))) {
                        curl_setopt($this->channel, $curlOption, $val);
                    }
                }
            }

            // Get additional headers.
            if ($this->headerSetCallback && is_callable($this->headerSetCallback)) {
                if (($items = call_user_func($this->headerSetCallback)) && is_array($items)) {
                    foreach ($items as $val) {
                        $this->headers[] = $val;
                    }
                }
            }

            // set http access headers.
            if (!empty($this->headers)) {
                curl_setopt($this->channel, CURLOPT_HTTPHEADER, $this->headers);
            }

            // create access api uri and set.
            $uri = $this->buildUri($endpoint, $query);
            curl_setopt($this->channel, CURLOPT_URL, $uri);

            // do curl exec.
            $response = curl_exec($this->channel);
            if (($eno = curl_errno($this->channel)) != CURLE_OK) {
                $err = curl_error($this->channel);
                curl_close($this->channel);
                throw new \RuntimeException(sprintf('lib curl error : %s (%s)', $err, $eno), 5000);
            }

            if (($type = curl_getinfo($this->channel, CURLINFO_CONTENT_TYPE)) != 'application/json') {
                curl_close($this->channel);
                throw new \RuntimeException(sprintf('invalid content type : %s', $type), 5001);
            }
            if (($code = curl_getinfo($this->channel, CURLINFO_HTTP_CODE)) != '200') {
                curl_close($this->channel);
                throw new \RuntimeException(sprintf('invalid status code : %s', $code), 5002);
            }

            if (is_string($response) && is_object(json_decode($response)) && (json_last_error() == JSON_ERROR_NONE)) {
                return json_decode($response, true);
            }
            throw new \RuntimeException('invalid response type', 5003);
        }

        /**
         * build uri
         *
         * @param $endpoint
         * @param $query
         * @return string
         */
        protected function buildUri($endpoint, $query = null)
        {
            $port = ($this->port) ? sprintf(':%s', $this->port) : '';
            $pathQuery = ($query) ? ('?' . $query) : '';
            return $this->url = $this->scheme . '://' . $this->host . $port . $this->path . $endpoint . $pathQuery;
        }
    }
}
