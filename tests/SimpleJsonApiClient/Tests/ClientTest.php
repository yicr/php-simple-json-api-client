<?php

namespace Yicr\SimpleJsonApiClient\Tests {

    use Yicr\SimpleJsonApiClient\Client;

    /**
     * Class ClientTest
     * @package     Yicr\SimpleJsonApiClient\Test
     * @access      public
     * @property    Client $client
     */
    class ClientTest extends \PHPUnit_Framework_TestCase
    {

        /**
         * setUp method
         *
         * @return void
         */
        public function setUp()
        {
            parent::setUp();
            $this->client = new Client('localhost');
        }

        /**
         * tearDown method
         *
         * @return void
         */
        public function tearDown()
        {
            unset($this->client);
            parent::tearDown();
        }

        public function testCheck()
        {
            $this->assertFalse($this->client->call());
        }
    }
}
