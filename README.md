# PHP JSON API Client

## Description
description.

## Requires
* PHP 5.3.3 or Higher

## Installation

include for `yicr/php-json-api-client` in your `composer.json` file. For example:

```bash
$ composer.phar require yicr/php-json-api-client
```

```json
{
    "require": {
        "yicr/php-json-api-client": "*"
    }
}
```

## Example

example

```
require __DIR__ . '/vendor/autoload.php';


use Yicr\SimpleJsonApiClient\Client;

try {
    $client = new Client('api.example.jp');
    $response = $client->call('/path/to/endpoint/');
} catch (Exception $ex) {
    //var_dump($ex);
}


```

Thank you.